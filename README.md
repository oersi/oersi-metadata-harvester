<h1>OER Metadta Harvester</h1>

The harvester module downloads the metadata from the other sources e.g. OAI-Server and converts them into JSON format. The JSON documents are transferred to the central oer-index via web services. The application starts daily automatically by a defined cron job. 
<hr />

<h3>Installation</h3>

*  Checking out the sources
    *  git clone `https://gitlab.com/oersi/oersi-metadata-harvester.git`
*  hange to the project directory
    *  cd ~/oersi-metadata-harvester

<hr />
<h3>Configuration</h3>

*  Set up the configuration directory
    * logback.xml - Logging-configuration
    * application.properties - configuration of the application
    * see example in envConf/default

* set configuration directory with envConfigDir=PATH 
    * for example via context.xml.default in Tomcat
>  
    <Context>
         <!-- path to the config-directory that contains all config-files of the application -->
        <Environment type="java.lang.String" name="envConfigDir" value="/some/path/conf/" override="false"/>
    </Context>
>  
*  or via Command-Line-Argument
    *  mvn -DskipTestes=true clean install
    *  mvn spring-boot:run

*  use `http://localhost:8080/templates/<year>-<month>.html` to call the statistics 
    *  for example : `http://localhost:8080/templates/2020-April.html `

 
<hr />
<h3>Technologies</h3>

*  springboot - The harvester module is a springboot application, provided as war file
*  Webservice (Spring-WS)
*  Test : JUnit 4
*  Slf4j (Logback)
*  HttpClient
*  Java 8
*  JSON-library
