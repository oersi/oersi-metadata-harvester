package oer.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OerImportConvertModulApplication{

	public static void main(String[] args) {
		SpringApplication.run(OerImportConvertModulApplication.class, args);
	}

}
