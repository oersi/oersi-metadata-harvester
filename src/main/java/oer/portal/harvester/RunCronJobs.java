package oer.portal.harvester;

import java.io.File;
import java.io.IOException;
import oer.portal.config.ApplicationProperties;
import oer.portal.converter.lom.rmif.ConvertLomToLRMIFormat;
import oer.portal.joai.client.JOAIClient;
import oer.portal.joai.exeption.OaiExeption;
import oer.portal.joai.util.JOAIUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RunCronJobs {

  private static Logger logger = LoggerFactory.getLogger(RunCronJobs.class);

  static String OER = "oer";
  static String FRAUENHOFER = "fraunhofer";

  @Autowired
  private ConvertLomToLRMIFormat convertLomToLRMIFormat;
  @Autowired
  private ApplicationProperties applicationProperties;

  @Value(value = "${arg.name}")
  private String oaiServerName;

  public String getOaiServerName() {
    return oaiServerName;
  }

  public void setOaiServerName(String oaiServerName) {
    this.oaiServerName = oaiServerName;
  }

  @Scheduled(cron = "${scheduling.job.cron}")
  public void excuteJoaiClient() throws IOException, InterruptedException, OaiExeption {

    logger.debug("metatdata location - {}", applicationProperties
        .getMetadataLocation());

    String[] serverNames = StringUtils.split(getOaiServerName(), ";");

    for (String string : serverNames) {
      String replaceToken = StringUtils.replace(string, "\\", "");
      String[] split = StringUtils.split(replaceToken, " ");
      String serverName = StringUtils.substringBefore(StringUtils.substringAfter(replaceToken,
          "-n"), " ");

      JOAIClient.callApplication(split);

      setConverter(serverName);
    }

    Thread.sleep(5000);
    JOAIUtils.deleteDirectoryRecursively(new File(applicationProperties.getMetadataLocation()
        .trim()));
  }

  private void setConverter(String serverName) {
    if (serverName.equals(OER)) {
      convertLomToLRMIFormat.getListOfJSONObjects();
    }
    if (serverName.equals(FRAUENHOFER)) {
      // call another converter!
    }
  }

}
