package oer.portal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "file:${envConfigDir:envConf/default/}configDirectoryLocation.properties")
public class ApplicationProperties {

  @Value("${configDirectoryLocation}")
  public String metadataLocation;

  public String getMetadataLocation() {
    return metadataLocation;
  }

  public void setMetadataLocation(String metadataLocation) {
    this.metadataLocation = metadataLocation;
  }
  
  @Value("${configStatisticsLocation}")
  public String statisticLocation;

  public String getStatisticLocation() {
    return statisticLocation;
  }

  public void setStatisticLocation(String statisticLocation) {
    this.statisticLocation = statisticLocation;
  }

}
