package oer.portal.joai.exeption;

public abstract class JoaiExeption {

  private JoaiExeption() {
    super();
  }

  public static final String CONTENTINVALID = "Metadaten-Inhalt ist ungültig!";
}
