package oer.portal.joai.validator;

import static joptsimple.util.RegexMatcher.regex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.ValueConverter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Validator {

  private static Logger LOG = LoggerFactory.getLogger(Validator.class);

  public static final String LOG_PATTERN = "[{}] [{}] [{}] {}";
  public static final String NOSET = "";

  private Properties confParameterProp = new Properties();

  private Properties configDirProp = new Properties();

  private String ROOT = "./";

  private static String statistics = null;

  public static String getStatistics() {
    return statistics;
  }

  public static void setStatistics(String statistics) {
    Validator.statistics = statistics;
  }

  private static String CONFIG_PARAMETER = "/configParameter.properties";
  private static String HELP_TXT = "help.txt";

  private static final String COFIG_DIRECTORY = "/configDirectoryLocation.properties";
  private static final String COFIG_STATISTICS_LOCATION = "configStatisticsLocation";

  private static final String CONFIG_RESOURCES = "./envConf/default";

  protected static File currentDirectory = null;
  protected static String supplierName = null;

  protected static String from = null;
  protected static String until = null;
  protected static String set = null;

  protected static String listset = null;
  protected static String listformat = null;
  protected static String record = null;
  protected static String listidentifier = null;
  protected static boolean token = false;

  private static final String N = "n";
  private static final String NAME = "name";
  private static final String F = "f";
  private static final String FROM = "from";
  private static final String U = "u";
  private static final String UNTIL = "until";
  private static final String S = "s";
  private static final String SET = "set";
  private static final String LISTSET = "listset";
  private static final String LISTFROMAT = "listformat";
  private static final String LISTIDENTIFIER = "listidentifier";
  private static final String RECORD = "record";
  private static final String H = "h";
  private static final String HELP = "help";
  private static final String P = "p";
  private static final String PATTERN = "pattern";

  public void isValid(String[] args) {

    reset();

    Set<Object> keys = null;

    try {

      setCurrentDirectory(new File(new File(ROOT).getCanonicalFile().toString()));

      confParameterProp.load(new FileInputStream(new File(configDirectories() + CONFIG_PARAMETER)));

      configDirProp.load(new FileInputStream(new File(configDirectories() + COFIG_DIRECTORY)));

      setStatistics(configDirProp.getProperty(COFIG_STATISTICS_LOCATION));

    } catch (FileNotFoundException e1) {
      e1.printStackTrace();

    } catch (IOException e1) {
      e1.printStackTrace();

    }

    try {

      keys = confParameterProp.keySet();

      ValueConverter<String> reg = regex(
          "([0-9]{4})-([0-9]{2})-([0-9]{2})([T][\\d]{2}\\:[\\d]{2}\\:[\\d]{2}[Z])?");

      OptionParser parser = new OptionParser();

      parser.accepts(N).withRequiredArg();

      parser.accepts(NAME).withRequiredArg();

      parser.accepts(F).withRequiredArg().withValuesConvertedBy(reg);

      parser.accepts(FROM).withRequiredArg().withValuesConvertedBy(reg);

      parser.accepts(U).withRequiredArg().withValuesConvertedBy(reg);

      parser.accepts(UNTIL).withRequiredArg().withValuesConvertedBy(reg);

      parser.accepts(S).withRequiredArg();

      parser.accepts(SET).withRequiredArg();

      parser.accepts(LISTSET).requiresArgument();

      parser.accepts(LISTFROMAT).requiresArgument();

      parser.accepts(LISTIDENTIFIER).requiresArgument();

      parser.accepts(RECORD).withRequiredArg();

      parser.accepts(HELP).forHelp();

      parser.accepts(H).forHelp();

      parser.accepts(PATTERN);

      parser.accepts(P);

      OptionSet options = parser.parse(args);

      if (options.has(HELP) || options.has(H)) {
        try {
          InputStream helpStream = this.getClass().getClassLoader().getResourceAsStream(HELP_TXT);
          String helpText = IOUtils.toString(helpStream);
          LOG.debug(helpText);
        } catch (IOException e) {
          e.getStackTrace();
        }
      }

      if (options.has(F)) {
        setFrom(options.valueOf(F).toString());
      }
      if (options.has(FROM)) {
        setFrom(options.valueOf(FROM).toString());
      }
      if (options.has(U)) {
        setUntil(options.valueOf(U).toString());
      }
      if (options.has(UNTIL)) {
        setUntil(options.valueOf(UNTIL).toString());
      }
      if (options.has(S)) {
        setSet(options.valueOf(S).toString());
      }
      if (options.has(SET)) {
        setSet(options.valueOf(SET).toString());
      }
      if (options.has(LISTFROMAT)) {
        setListformat(LISTFROMAT);
      }
      if (options.has(LISTSET)) {
        setListset(LISTSET);
      }
      if (options.has(LISTIDENTIFIER)) {
        setListidentifier(LISTIDENTIFIER);
      }
      if (options.has(RECORD)) {
        setRecord(options.valueOf(RECORD).toString());
      }
      if (options.has(PATTERN)) {
        setTokenPattern(true);
      }
      if (options.has(P)) {
        setTokenPattern(true);
      }
      if (options.has(N)) {
        setSupplierName(options.valueOf(N).toString());
      }
      if (options.has(NAME)) {
        setSupplierName(options.valueOf(NAME).toString());
      }

    } catch (Exception e) {

      getExeptionMessage(args, keys, e);
    }

  }

  public static String configDirectories() {
    return CONFIG_RESOURCES;
  }

  public static File getCurrentDirectory() {
    return currentDirectory;
  }

  public static void setCurrentDirectory(File currentDirectory) {
    Validator.currentDirectory = currentDirectory;
  }

  public Properties getProperties() {
    return confParameterProp;
  }

  public void setProperties(Properties properties) {
    this.confParameterProp = properties;
  }

  public static String getFrom() {
    return from;
  }

  public static void setFrom(String from) {
    Validator.from = from;
  }

  public static String getUntil() {
    return until;
  }

  public static void setUntil(String until) {
    Validator.until = until;
  }

  public static String getSet() {
    return set;
  }

  public static void setSet(String set) {
    Validator.set = set;
  }

  public static String getListformat() {
    return listformat;
  }

  public static void setListformat(String listformat) {
    Validator.listformat = listformat;
  }

  public static String getSupplierName() {
    return supplierName;
  }

  public static void setSupplierName(String supplierName) {
    Validator.supplierName = supplierName;
  }

  public static String getRecord() {
    return record;
  }

  public static void setRecord(String record) {
    Validator.record = record;
  }

  public static boolean isTokenPattern() {
    return token;
  }

  public static String getListset() {
    return listset;
  }

  public static void setListset(String listset) {
    Validator.listset = listset;
  }

  public static String getListidentifier() {
    return listidentifier;
  }

  public static void setListidentifier(String listidentifier) {
    Validator.listidentifier = listidentifier;
  }

  public void setTokenPattern(boolean token) {
    Validator.token = token;
  }

  private void getExeptionMessage(String[] args, Set<Object> keys, Exception e) {

    String exeption = "";

    for (int i = 0; i < args.length; i++) {
      exeption = exeption + " " + args[i];
    }

    LOG.error("Fehler: " + e.getMessage() + "\n");

    LOG.error("Bitte prüfen Sie Ihre Eingaben!: Suppliernamen:" + StringUtils
        .replace(keys.toString(), "configDirectoryLocation,", "")
        .replace("configStatisticsLocation,", "")
        + "\n" + "Ihre Eingaben: [" + exeption + " ]");
  }

  private void reset() {

    setFrom(null);
    setUntil(null);
    setSet(null);
    setListset(null);
    setListformat(null);
    setRecord(null);
    setListidentifier(null);
    setTokenPattern(false);
  }

  @Override
  public String toString() {
    return "Validator [supplierName=" + supplierName + ", from=" + from + ", until=" + until
        + ", set=" + set
        + ", listset=" + listset + ", listformat=" + listformat + "]";
  }

}
