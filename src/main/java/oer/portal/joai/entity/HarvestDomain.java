package oer.portal.joai.entity;

public interface HarvestDomain {

  public boolean connect(String serverAddresse, String from, String until,
      String metadataPrefix, String listCollection, String set);

}
