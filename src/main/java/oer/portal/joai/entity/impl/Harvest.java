package oer.portal.joai.entity.impl;

import oer.portal.joai.entity.HarvestDomain;
import oer.portal.joai.service.HarvestService;
import oer.portal.joai.util.OaiPmhConstants;
import oer.portal.joai.util.SupplierData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Harvest implements HarvestService {

  private static Logger LOG = LoggerFactory.getLogger(Harvest.class);

  public final boolean harvesting(String url, String from, String until, String metadataPrefix,
      String recordCollection, String set) {

    HarvestDomain oaiPmh = new OaiPmhIpml();
    boolean finished = false;

    try {

      finished = oaiPmh
          .connect(url, from, until, metadataPrefix, OaiPmhConstants.getListRecordCollection(),
              set);

    } catch (Exception e) {
      LOG.error(e.getMessage());
    }
    return finished;

  }

  public void harvesting(String url, String string, String string2, String metadatePrefix,
      String set) {
    // TODO Auto-generated method stub

  }

  public void supplierData(SupplierData supplierData) {
    // TODO Auto-generated method stub

  }

}
