package oer.portal.joai.entity.impl;

import oer.portal.joai.connector.HttpConnector;
import oer.portal.joai.exeption.JoaiExeption;
import oer.portal.joai.service.HarvestService;
import oer.portal.joai.util.MetadataValidator;
import oer.portal.joai.util.SupplierData;
import oer.portal.joai.util.Writer;
import oer.portal.joai.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Records extends Validator {

  public static Logger LOG = LoggerFactory.getLogger(Records.class);

  protected boolean finished = false;

  private static final String NORECORD = "noRecordsMatch";
  private static final String CODE = "code=";

  private String getRecords(String url, String from, String until, String metadataPrefix,
      String set) {

    String setValue = (set == null) ? NOSET : set;

    HarvestService harvestService = new Harvest();

    int i = 0;

    Writer writer = new Writer();

    do {

      HttpConnector.setRequestValue(true);

      finished = harvestService.harvesting(url, from, until, metadataPrefix, set, set);

      i++;

    } while (finished = true && i < 1);

    if (HttpConnector.isDataContentInfo()) {

      String result = "";

      for (String string : HttpConnector.getInfoMessage()) {
        if (!StringUtils.isEmpty(string)) {
          result += string.trim() + ", ";
        }
      }

      LOG.info(LOG_PATTERN, getSupplierName(), setValue, "BILANZ",
          StringUtils.removeEnd(result.trim(), ","));

      writer.statistics(HttpConnector.getInfoMessage(), false, "");

    } else if (!HttpConnector.isDataContentInfo()) {

      String errorCode = "";

      if (!StringUtils.contains(MetadataValidator.getMessage(), JoaiExeption.CONTENTINVALID)) {

        errorCode = StringUtils.substringBetween(MetadataValidator.getMessage(), CODE, ">")
            .replace("\"", "");

      }

      if (StringUtils.contains(errorCode, NORECORD)) {

        String norecord = "";

        LOG.info(LOG_PATTERN, getSupplierName(), setValue, errorCode, "");

        for (String string : HttpConnector.getInfoMessage()) {
          if (!StringUtils.isEmpty(string)) {
            norecord += string.trim() + ", ";
          }
        }
        LOG.debug(LOG_PATTERN, getSupplierName(), setValue, errorCode,
            StringUtils.removeEnd(norecord.trim(), ","));

      } else {

        if (!StringUtils.isEmpty(errorCode)) {
          LOG.error(LOG_PATTERN, getSupplierName(), setValue, errorCode, "");
        }

        String error = "";

        for (String string : HttpConnector.getInfoMessage()) {
          if (!StringUtils.isEmpty(string)) {
            error += string.trim() + ", ";
          }
        }
        LOG.debug(LOG_PATTERN, getSupplierName(), setValue, errorCode,
            StringUtils.removeEnd(error.trim(), ","));
      }

      writer.statistics(HttpConnector.getInfoMessage(), true, "");
    }

    return null;
  }

  public void supplierDetails(SupplierData supplierData) {

    getRecords(supplierData.getUrl(), supplierData.getFrom(), supplierData.getUntil(),
        supplierData.getMetadataPrefix(), supplierData.getSet());
  }
}
