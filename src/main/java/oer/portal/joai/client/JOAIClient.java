package oer.portal.joai.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import oer.portal.joai.connector.HttpConnector;
import oer.portal.joai.exeption.OaiExeption;
import oer.portal.joai.util.JoaiIntializer;
import oer.portal.joai.util.OaiPmhConstants;
import oer.portal.joai.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JOAIClient extends JOAI {

  private static Logger LOG = LoggerFactory.getLogger(JOAIClient.class);

  private static HttpConnector conector = null;

  private static String[] argument;

  private static final String MULTISET = "multiset";

  private static final String SLASH = "/";

  private static final String UNDERSCORE = "_";

  private static String NO_MULTISET = "Kein Multiset-Eintrag in der Properties-Datei!.....";

  private static final String MULTISETS_FROM_PROPERTIES = "/configMultiset.properties";

  /**
   * @param args
   * @throws IOException
   */

  protected static Validator validator = null;

  private static Properties properties = new Properties();


  public static void callApplication(String[] args) throws IOException, OaiExeption {
    
    LOG.debug("Version: " + JoaiIntializer.getApplicationVersionEntries() + "(" + JoaiIntializer
        .getApplicationRevisionEntries() + ")");

    setArgument(args);

    validator = new Validator();
    validator.isValid(args);

    if (StringUtils.equals(getSet(), MULTISET)) {
      multisetsProperties();
    } else {
      multisetsTerminal();
    }
  }

  @SuppressWarnings("static-access")
  private static void run() throws OaiExeption {

    startApplication();
    conector.setIndex(1);

    if (getListidentifier() != null) {

      OaiPmhConstants.setListRecordCollection(OaiPmhConstants.getListIdentifiers());

    } else {

      OaiPmhConstants.setListRecordCollection(OaiPmhConstants.newlistRecordcollection());
    }

  }

  public static void multisetsTerminal() throws OaiExeption {

    if ((getListset() != null || getListformat() != null)
        || (getFrom() == null && getUntil() == null) && (getRecord() == null && getSet() == null)) {

      setValuesFromTerminal(null);
      run();

    } else if (getSet() != null || getListidentifier() != null) {

      if (getSet() != null) {

        String[] splitArr = StringUtils.split(getSet(), ',');
        for (String string : splitArr) {
          setValuesFromTerminal(string);
          run();
        }
      } else {

        setValuesFromTerminal(null);
        run();
      }

    } else {

      setValuesFromTerminal(null);
      run();
    }

  }

  public static void multisetsProperties() throws IOException, OaiExeption {

    if (StringUtils.equals(getSet(), MULTISET)) {

      try {
        properties.load(new FileInputStream(
            new File(configDirectories() + MULTISETS_FROM_PROPERTIES)));

        String temp = (String) properties.get(getSupplierName());

        if (!StringUtils.equals(temp, null)) {
          String[] splitArr = StringUtils.split(temp, ',');
          for (String string : splitArr) {
            setValuesFromTerminal(string);
            run();
          }
        } else {
          LOG.error(NO_MULTISET);
        }

      } catch (FileNotFoundException e1) {
        LOG.error(e1.getMessage());
        e1.printStackTrace();
      } catch (IOException e1) {

        LOG.error(e1.getMessage());
        e1.printStackTrace();
      }

    }

  }


  public String buildSupplierDir(String dir) throws OaiExeption {
    String path = dir + getSupplierName() + UNDERSCORE;
    
    String locationPath = path + "0" + SLASH;
    
    if(getFrom() != null || getUntil() != null) {

      return mackLocation(path);
      
    }else if (getSet() != null) {
      mkdir(locationPath);
      return getSetNameDirectory(locationPath);
    } else {
      return locationPath;
    }

  }

  public String mackLocation(String path) throws OaiExeption {
    String locationPath = null;

    if (getFrom() == null && getUntil() == null) {
      locationPath = path + "0" + SLASH;
    } else if (getFrom() == null && getUntil() != null) {
      locationPath = path + "0" + UNDERSCORE + getUntil()+ SLASH;
    } else if (getFrom() != null && getUntil() == null) {
      locationPath = path + getFrom() + SLASH;
    } else if (getFrom() != null && getUntil() != null) {
      locationPath = path + getFrom() + UNDERSCORE + getUntil() + SLASH;
    }
    mkdir(locationPath);

    return getSetNameDirectory(locationPath);
  }

  private void mkdir(String locationPath) {
    setMakeSupplierDir(new File(locationPath));
    getMakeSupplierDir().mkdir();
  }

  private String getSetNameDirectory(String locationPath) throws OaiExeption {
    if (getValueFromTerminal() != null) {
      locationPath = locationPath + JOAIClient.urlEncode(getValueFromTerminal()) + SLASH;
      LOG.debug("SetNameDirectory: {}",locationPath);
      return locationPath;
    }
    return locationPath;
  }

  protected static String[] getArguments() {
    return argument;
  }

  public static void setArgument(String[] argument) {
    JOAIClient.argument = argument;
  }

}
