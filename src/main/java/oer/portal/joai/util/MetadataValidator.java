package oer.portal.joai.util;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import oer.portal.joai.exeption.JoaiExeption;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetadataValidator {

  private MetadataValidator() {
    super();
  }

  public static final Logger logger = LoggerFactory.getLogger(MetadataValidator.class);
  public static final String LOG_PATTERN = "[{}] [{}] [{}] {}";

  private static final String ERROR = "<error";
  private static final String RESUMPTIONTOKEN = "resumptionToken=";
  private static final String NO_RECORD_MACH = "noRecordsMatch";
  private static final String LIST_RECORD = "<ListRecords";
  private static final String LIST_IDENTIFIER = "<ListIdentifiers";
  private static final String LIST_FORMATS = "<ListMetadataFormats";
  private static final String LIST_SETS = "<ListSets";
  private static final String ENDLISTRECORDS = "<ListRecords/>";
  private static final String RECORD = "<record>";

  static boolean flage = false;

  public static boolean isFlage() {
    return flage;
  }

  public static void setFlage(boolean flage) {
    MetadataValidator.flage = flage;
  }

  private static String token = null;

  private static String message = "";

  public static String getResumptionTokenValue(String des, String pattern) {

    File sourceFile = new File(des);

    String content;

    try {
      content = FileUtils.readFileToString(sourceFile);

      Matcher matcher = Pattern.compile(pattern, Pattern.DOTALL).matcher(content);

      if (matcher.find()) {
        token = matcher.group(1);
      }

    } catch (IOException e) {
      logger.error("IOExeption", e);
    }

    return urlEncoder(StringUtils.replace(token, "\n", ""));
  }

  public static boolean getError(String des, String supplierName, String set) {

    File sourceFile = new File(des);
    String content;
    try {
      content = FileUtils.readFileToString(sourceFile);

      String p1 = ".*(<error *.*>(.*)|";
      String p2 = "<ListIdentifiers>(.*)</ListIdentifiers>|";
      String p3 = "<ListRecords *.*>(.*)<record>(.*)|";
      String p4 = "<ListMetadataFormats *.*>(.*)<metadataFormat>|";
      String p5 = "<ListSets *.*>(.*)<set>).*";

      Matcher matcher = Pattern.compile(p1 + p2 + p3 + p4 + p5, Pattern.DOTALL).matcher(content);

      if (matcher.find()) {

        String find = matcher.group(1);

        return isValid(supplierName, set, content, matcher, find);
      } else {
        return invalidResponse(supplierName, set, content);
      }
    } catch (IOException e) {
      logger.error("Exeption", e);
    }
    return false;
  }

  private static boolean invalidResponse(String supplierName, String set, String content) {
    if (StringUtils.contains(content, ENDLISTRECORDS) && StringUtils.isNotEmpty(content)) {
      setFlage(true);
      return true;
    } else {
      logger.error(LOG_PATTERN, supplierName, set, "INVALID_RESPONSE",
          JoaiExeption.CONTENTINVALID + ": ", content);
      setMessage(JoaiExeption.CONTENTINVALID);
      return false;
    }
  }

  private static boolean isValid(String supplierName, String set, String content,
      Matcher matcher, String find) {
    if (StringUtils.contains(find, ERROR)) {
      String group = matcher.group(1);
      if (StringUtils.contains(content, ERROR) && StringUtils
          .contains(content, RESUMPTIONTOKEN)) {
        setFlage(true);
        return true;
      }
      if (StringUtils.contains(find, NO_RECORD_MACH)) {

        logger.info(LOG_PATTERN, supplierName, set, NO_RECORD_MACH, group);
      } else {

        logger.error(LOG_PATTERN, supplierName, set, "ERROR", group);
      }
      setMessage(matcher.group(1));

      return false;
    } else if ((StringUtils.isNotEmpty(content)
        && ((StringUtils.contains(find, LIST_RECORD) && StringUtils.contains(find, RECORD))
            || StringUtils.contains(find, LIST_IDENTIFIER)
            || StringUtils.contains(find, LIST_FORMATS)
            || StringUtils.contains(find, LIST_SETS)))) {

      return true;
    }
    return false;
  }

  /**
   * OAI-PMH Before including a resumptionToken in the URL of a subsequent request, a harvester must
   * encode any special characters in it.
   *
   * @param code
   * @return
   */
  public static String urlEncoder(String url) {

    String[] d = {"%"};
    String[] e = {"%25"};

    String temp = StringUtils.replaceEach(url, d, e);

    String[] a = {"/", "?", "#", "=", "&", ":", ";", " ", "+", "|"};
    String[] b = {"%2F", "%3F", "%23", "%3D", "%26", "%3A", "%3B", "%20", "%2B", "%7C"};

    return StringUtils.replaceEachRepeatedly(temp, a, b);
  }

  public static String getMessage() {
    return message;
  }

  public static void setMessage(String message) {
    MetadataValidator.message = message;
  }

}
