package oer.portal.joai.util;

public class SupplierData {

  public String url = "";
  public String from = "";
  public String until = "";
  public String metadatePrefix = "";
  public String set = "";
  public String listSet = "";
  public String listFormat = "";
  public String record = "";

  public SupplierData(String url, String name, String from, String until,
      String metadatePrefix, String set) {
    super();
    this.url = url;
    this.from = from;
    this.until = until;
    this.metadatePrefix = metadatePrefix;
    this.set = set;
  }

  public String getListFormat() {
    return listFormat;
  }

  public void setListFormat(String listFormat) {
    this.listFormat = listFormat;
  }

  public String getListSet() {
    return listSet;
  }

  public void setListSet(String listSet) {
    this.listSet = listSet;
  }

  public String getRecord() {
    return record;
  }

  public void setRecord(String record) {
    this.record = record;
  }

  public String getSet() {
    return set;
  }

  public void setSet(String set) {
    this.set = set;
  }

  public String getUntil() {
    return until;
  }

  public void setUntil(String until) {
    this.until = until;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getMetadataPrefix() {
    return metadatePrefix;
  }

  public void setMetadataPrefix(String metadatePrefix) {
    this.metadatePrefix = metadatePrefix;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public SupplierData() {
  }

}
