package oer.portal.joai.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JOAIUtils {
  
  private JOAIUtils() {
    throw new IllegalStateException("Utility class");
  }

  private static Logger LOG = LoggerFactory.getLogger(JOAIUtils.class);

  protected static File makeSupplierDir;
  private static long time = 0;
  private static String configPattern = null;

  public static void configLocation(String config) {

    try {
      OaiPmhConstants.setMetadataDirectory(config);
    } catch (Exception e) {
    }
  }

  public static String time(long start) {

    long diffMillis = new GregorianCalendar().getTimeInMillis() - start;
    double diffDay = diffMillis / 1000D / 60D / 60D / 24D;
    double diffHour = diffDay % 1 * 24;
    double diffMin = diffHour % 1 * 60;
    double diffSec = diffMin % 1 * 60;
    DecimalFormat format = new DecimalFormat("#0.00");

    return (long) diffHour + ":" + (long) diffMin + ":" + format.format(diffSec);
  }

  public static String getConfigPattern() {
    return configPattern;
  }

  public static void setTime(final long time) {
    JOAIUtils.time = time;
  }

  public static void setConfigPattern(String configPattern) {
    JOAIUtils.configPattern = configPattern;
  }

  public static long getTime() {
    return JOAIUtils.time;
  }

  public static void deleteDirectoryRecursively(File dir) {

    try {
      File[] files = dir.listFiles();
      if (files != null) {
        for (int i = 0; i < files.length; i++) {
          if (files[i].isDirectory()) {
            deleteDirectoryRecursively(files[i]);
          } else {
            files[i].delete();
          }
        }

        dir.delete(); // Ordner löschen
      }
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static void deleteDir(File dir) {

    try {
      File[] files = dir.listFiles();
      if (files != null) {
        for (int i = 0; i < files.length; i++) {
          if (files[i].isDirectory()) {
            deleteDir(files[i]);

          } else {
          }
        }
        dir.delete();
      }
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static void copy(File file, File ziel) throws FileNotFoundException, IOException {
    BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));
    BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(ziel, true));
    int bytes = 0;
    while ((bytes = inputStream.read()) != -1) {
      try {
        outStream.write(bytes);
      } catch (IOException e) {
        LOG.error("Error creating file!" + e);
      }
    }
    inputStream.close();
    outStream.close();
  }

  public static String decode(String url) throws UnsupportedEncodingException {
    String prevURL = "";
    String decodeURL = url;
    while (!prevURL.equals(decodeURL)) {
      prevURL = decodeURL;
      decodeURL = URLDecoder.decode(decodeURL, "UTF-8");
    }
    return decodeURL;

  }

}
