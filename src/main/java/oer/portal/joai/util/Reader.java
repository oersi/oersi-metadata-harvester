package oer.portal.joai.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reader {

  private static Logger LOG = LoggerFactory.getLogger(Reader.class);

  public String readFile(String file) {

    BufferedReader br = null;

    try {
      FileReader fr = new FileReader(file);
      br = new BufferedReader(fr);

      return br.readLine();

    } catch (IOException e) {
      LOG.error("Fehler beim Lesen der Datei!" + e);
    } finally {
      try {
        br.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return "";

  }

}
