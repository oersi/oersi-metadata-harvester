package oer.portal.joai.util;

import static java.nio.file.Files.delete;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteEmptyFolder {
  
  private static Logger logger = LoggerFactory.getLogger(DeleteEmptyFolder.class);

  private final List<File> emptyFolders;

  public DeleteEmptyFolder(String path) {
    this(new File(path));
  }

  public DeleteEmptyFolder(File dir) {
    emptyFolders = new ArrayList<>();
    listEmptyFolders(dir);
  }

  public void deleteEmptyFolder() {
    for (File emptyDir : emptyFolders) {
      deleteFolder(emptyDir);
    }
  }

  private void deleteFolder(File dir) {
    if (dir.isDirectory()) {
      for (File file : dir.listFiles()) {
        deleteFolder(file);
      }
      try {
        delete(dir.toPath());
      } catch (IOException e) {
        logger.error("file not found! {} ", e);
      }
    }
  }

  private void listEmptyFolders(File dir) {
    if (isEmpty(dir)) {
      emptyFolders.add(dir);
    } else {
      for (File file : dir.listFiles()) {
        if (file.isDirectory()) {
          listEmptyFolders(file);
        }
      }
    }
  }

  private boolean isEmpty(File dir) {
    if (dir.isDirectory()) {
      for (File file : dir.listFiles()) {
        if (file.isDirectory()) {
          if (!isEmpty(file)) {
            return false;
          }
        } else {
          return false;
        }
      }
      return true;
    }
    return false;
  }

}