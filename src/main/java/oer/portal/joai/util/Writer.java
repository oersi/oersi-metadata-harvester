package oer.portal.joai.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import oer.portal.joai.validator.Validator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Writer extends Validator {

  private static Logger LOG = LoggerFactory.getLogger(Writer.class);

  private final DateFormat inhtml = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
  private static final String JAVASCRIPT = "statistics.js";

  private static final String DOCTYPE = "<!DOCTYPE html>";

  private static final String TR = "<tr>";
  private static final String TR_ERROR = "<tr class=\"danger\">";
  private static final String TD = "<td>";
  private static final String TD_ERROR = "<td class=\"text-danger\">";
  private static final String RESULT_ERROR = "<td class=\"text-danger\"><span class=\"glyphicon glyphicon-remove\"></span></td>";
  private static final String RESULT_OK = "<td class=\"text-success\"><span class=\"glyphicon glyphicon-ok\"></span></td>";
  private static final String TD_END = "</td>";
  private static final String TR_END = "</tr>\n";

  private static final String ERROR = "<error";

  public String statistics(String[] messageContent, boolean errorflag, String errormessage) {

    String destination = getStatistics().trim();

    FileWriter fileWriter = null;

    try {

      Date currentTime = new Date();

      Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
      calendar.setTime(currentTime);

      final DateFormat dfIso = new SimpleDateFormat("yyyy-MMMM");

      String file = destination + dfIso.format(calendar.getTime()) + ".html";

      fileWriter = new FileWriter(file, true);

      Reader fileReader = new Reader();

      if (!StringUtils.contains(fileReader.readFile(file), DOCTYPE)) {
        fileWriter.write(htmlStyle() + "\n");
      }

      if (errorflag) {
        fileWriter.write(TR_ERROR);
      } else {
        fileWriter.write(TR);
      }

      fileWriter.write(TD + inhtml.format(calendar.getTime()) + TD_END);

      for (String str : messageContent) {
        String value = (str == null) ? "" : str.split(" = ")[1];
        fileWriter.write(TD + value + TD_END);
      }

      String message = "";

      if (StringUtils.contains(MetadataValidator.getMessage(), ERROR)) {
        String massageValue = StringUtils
            .substringBetween(MetadataValidator.getMessage(), "code=", ">");
        message = (massageValue == null) ? "" : massageValue;
      } else {
        message = MetadataValidator.getMessage();
      }

      fileWriter.write(TD_ERROR + message + " " + errormessage + TD_END);

      if (errorflag) {
        fileWriter.write(RESULT_ERROR);
      } else {
        fileWriter.write(RESULT_OK);
      }

      MetadataValidator.setMessage("");

      fileWriter.write(TR_END);

      copyJavascript(destination);

    } catch (IOException e) {
      LOG.error("Dokument kann nicht erstellt werden! ", e);
    } finally {
      if (fileWriter != null) {
        try {
          fileWriter.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return "";
  }

  private void copyJavascript(String destination) throws IOException {
    InputStream jsStream = this.getClass().getClassLoader().getResourceAsStream(JAVASCRIPT);
    File jsFile = new File(destination + JAVASCRIPT);
    if (!jsFile.exists()) {
      FileUtils.copyInputStreamToFile(jsStream, new File(destination + JAVASCRIPT));
    }
  }

  public static String htmlStyle() {

    String style = DOCTYPE + "\n" + "<html>\n" + "<head>\n"
        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n"
        + "<link href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
        + "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.3/css/theme.bootstrap.min.css\" />\n"
        + "<script src=\"http://code.jquery.com/jquery-3.1.1.min.js\" type=\"text/javascript\"></script>\n"
        + "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.3/js/jquery.tablesorter.combined.min.js\"></script>\n"
        + "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.3/js/widgets/widget-cssStickyHeaders.min.js\"></script>\n"
        + "<script src=\"" + JAVASCRIPT + "\" type=\"text/javascript\"></script>\n"
        + "<style>  tr.group td { font-weight: bold; cursor: pointer;} tr.group { background:lightgrey; } </style>\n"
        + "</head>\n"
        + "<body class=\"container-fluid\">\n"
        + "<h1>JOAI-Client <small>Statistik zu den abgeholten Metadaten</small></h1>"
        + "<table class=\"table\">" + "<thead>\n" + "<tr>"
        + "<th>Datum</th>" + "<th>Collection</th>" + "<th>Count</th>" + "<th>from</th>"
        + "<th>until</th>"
        + "<th>set</th>" + "<th>format</th>" + "<th>Message</th>" + "<th>Process</th>" + "</tr>"
        + "</thead><tbody>\n";

    return style;

  }

}
