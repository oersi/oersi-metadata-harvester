package oer.portal.joai.util;

public abstract class OaiPmhConstants {

  private OaiPmhConstants() {
    super();
  }

  static String metadataDirectory = null;

  static String LISTRECORDS = "=ListRecords&metadataPrefix=";
  static String LISTIDENTIFIERS = "=ListIdentifiers&metadataPrefix=";

  static String LIST_RECORD_COLLECTION = "=ListRecords&resumptionToken=";
  static String METADATAFILENAME = null;

  static String NEW_LIST_RECORD_COLLECTION = "=ListRecords&resumptionToken=";

  static String LISTSETCOLLECTION = "=ListSets&resumptionToken=";

  static String LISTIDENTIFIERCOLLECTION = "=ListIdentifiers&resumptionToken=";

  public static String getListIdentifiers() {
    return LISTIDENTIFIERS;
  }

  public static void setListIdentifiers(String listIdentifiers) {
    OaiPmhConstants.LISTIDENTIFIERS = listIdentifiers;
  }

  public static String getListSetCollection() {
    return LISTSETCOLLECTION;
  }

  public static void setListSetCollection(String listSetCollection) {
    OaiPmhConstants.LISTSETCOLLECTION = listSetCollection;
  }

  public static String getListIdentifierCollection() {
    return LISTIDENTIFIERCOLLECTION;
  }

  public static void setListIdentifierCollection(String listIdentifierCollection) {
    OaiPmhConstants.LISTIDENTIFIERCOLLECTION = listIdentifierCollection;
  }

  public static String getMetadataDirectory() {
    return metadataDirectory;
  }

  public static void setMetadataDirectory(String metadataDirectory) {
    OaiPmhConstants.metadataDirectory = metadataDirectory;
  }

  public static String getMetadatFileName() {
    return METADATAFILENAME;
  }

  public static void setMetadatFileName(String location) {
    METADATAFILENAME = location;
  }

  public static String getListRecordCollection() {
    return LIST_RECORD_COLLECTION;
  }

  public static void setListRecordCollection(String recordCollection) {
    LIST_RECORD_COLLECTION = recordCollection;
  }

  public static String getListrecords() {
    return LISTRECORDS;
  }

  public static String newlistRecordcollection() {
    return NEW_LIST_RECORD_COLLECTION;
  }

}
