package oer.portal.converter.lom.rmif;

public abstract class OAIElementName {

  public static final String LISTRECORDS = "ListRecords";
  public static final String RECORD = "record";
  public static final String METADATA = "metadata";
  public static final String LOM = "lom";
  public static final String GENERAL = "general";
  public static final String LIFECYCLE = "lifeCycle";
  public static final String CONTRIBUTE = "contribute";
  public static final String ENTITY = "entity";
  public static final String TITLE = "title";
  public static final String DESCRIPTION = "description";
  public static final String KEYWORDS = "keywords";
  public static final String KEYWORD = "keyword";
  public static final String CLASSIFICATION = "classification";
  public static final String STRING = "string";
  public static final String LANGUAGE = "language";
  public static final String EDUCATIONAL = "educational";
  public static final String LEARNINGRESOURCETYPE = "learningResourceType";
  public static final String VALUE = "value";
  public static final String RIGHTS = "rights";
  public static final String VERSION = "version";
  public static final String HEADER = "header";
  public static final String IDENTIFIER = "identifier";
}
