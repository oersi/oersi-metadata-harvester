package oer.portal.converter.lom.rmif;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import oer.portal.config.ApplicationProperties;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConvertLomToLRMIFormat {

  final Logger logger = LoggerFactory.getLogger(ConvertLomToLRMIFormat.class);

  private static final String NAMESPACE = "http://ltsc.ieee.org/xsd/LOM";

  @Autowired
  private ApplicationProperties applicationProperties;

  List<JSONObject> jsonObjects;

  Namespace nodeNamespace;

  public List<JSONObject> getListOfJSONObjects() {

    SAXBuilder builder = new SAXBuilder();

    File dir = new File(applicationProperties.getMetadataLocation().trim());

    jsonObjects = new ArrayList<>();

    for (File child : listFileTree(dir)) {

      JSONObject mainJsonObject = null;
      Map<String, String> dataMap = null;

      try {

        Document document = builder.build(child);
        Element rootNode = document.getRootElement();
        Namespace rootNamespace = rootNode.getNamespace();
        List<Element> list = rootNode.getChild(OAIElementName.LISTRECORDS, rootNamespace)
            .getChildren(
                OAIElementName.RECORD, rootNamespace);

        JSONArray nameArray = null;

        ArrayList<String> elementValueList = null;

        for (int i = 0; i < list.size(); i++) {

          Element node = list.get(i);

          nodeNamespace = node.getNamespace(NAMESPACE);

          Element nodeLomElement = node.getChild(OAIElementName.METADATA, rootNamespace).getChild(
              OAIElementName.LOM, nodeNamespace);
          Element nodeGeneralElements = nodeLomElement.getChild(OAIElementName.GENERAL,
              nodeNamespace);

          nameArray = new JSONArray();

          dataMap = new HashMap<>();

          dataMap.put("data", "LRMI");

          mainJsonObject = new JSONObject(dataMap);

          // author - Element
          putElementAuthor(mainJsonObject, nameArray, nodeLomElement);

          // title - element
          putElementTitle(mainJsonObject, nodeGeneralElements);

          // description - element
          putElement(mainJsonObject, nodeGeneralElements, OAIElementName.DESCRIPTION, "abstract");

          // keywords       
          elementValueList = new ArrayList<>();

          putElementKeywords(mainJsonObject, elementValueList, nodeGeneralElements);

          // classification - element
          putElementClassification(mainJsonObject, elementValueList, nodeLomElement);

          // language - element
          putElement(mainJsonObject, nodeGeneralElements, OAIElementName.LANGUAGE, "inLanguage");

          // learningResourceType - element
          learningResourceType(mainJsonObject, nodeLomElement);

          // rights
          putElementRight(mainJsonObject, nodeLomElement);

          //version
          putElementVersion(mainJsonObject, nodeLomElement);

          jsonObjects.add(mainJsonObject);

        }
      } catch (IOException | JDOMException e) {
        logger.error(e.getMessage());
      }

    }
    return jsonObjects;
  }

  private void putElementAuthor(JSONObject mainJsonObject, JSONArray nameArray,
      Element nodeLomElement) {

    Element contribute = nodeLomElement.getChild(OAIElementName.LIFECYCLE,
        nodeNamespace);
    try {
      if (contribute != null) {

        Element con = contribute.getChild(
            OAIElementName.CONTRIBUTE, nodeNamespace);
        if (con != null) {

          List<Element> entityValue = con.getChildren(OAIElementName.ENTITY,
              nodeNamespace);
          if (entityValue != null) {
            putValueOfName(mainJsonObject, nameArray, entityValue);
          }
        }
      }
    } catch (Exception e) {
      logger.debug(e.toString());
    }

  }

  private void putValueOfName(JSONObject mainJsonObject, JSONArray nameArray,
      List<Element> entityValue) {
    Map<String, String> nameMap;
    try {
      for (Element element : entityValue) {
        if (element != null) {
          String convert = element.getValue().replaceAll("\r", " ").replaceAll("\n", " ");
          String[] value = null;
          if (convert != null && convert.length() > 0) {
            value = StringUtils.substringsBetween(convert, " N:", "FN:");

            nameMap = new HashMap<>();
            putElement(nameMap, value);
            nameArray.put(nameMap);
            mainJsonObject.put("author", nameArray);

          }

        }
      }
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElement(Map<String, String> nameMap, String[] value) {
    String givenName = null;
    String famileName = null;
    try {
      if (value != null && value.length > 0) {
        givenName = StringUtils.substringBefore(value[0], ";");
        if (givenName != null && givenName.length() > 0) {
          nameMap.put("givenName", givenName);
        }
        famileName = StringUtils.substringAfter(value[0], ";");
        if (famileName != null && famileName.length() > 0) {
          nameMap.put("famileName", famileName);
        }
      }
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElementTitle(JSONObject mainJsonObject, Element nodeGeneralElements) {
    JSONArray titleArry = null;
    titleArry = new JSONArray();
    try {
      for (Element element : nodeGeneralElements.getChildren(OAIElementName.TITLE,
          nodeNamespace)) {
        titleArry.put(element.getValue().trim());
      }
      mainJsonObject.put("title", titleArry);
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElementKeywords(JSONObject mainJsonObject, ArrayList<String> elementValueList,
      Element nodeGeneralElements) {
    try {
      List<Element> keywords = nodeGeneralElements.getChild(OAIElementName.KEYWORD,
          nodeNamespace)
          .getChildren(
              OAIElementName.STRING, nodeNamespace);
      for (Element element : keywords) {
        elementValueList.add(element.getValue().trim());
      }
      mainJsonObject.put(OAIElementName.KEYWORDS, elementValueList);
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElementClassification(JSONObject mainJsonObject,
      ArrayList<String> elementValueList, Element nodeLomElement) {
    try {
      Element classificationKeyword = nodeLomElement.getChild(
          OAIElementName.CLASSIFICATION, nodeNamespace);
      if (classificationKeyword != null) {
        Element keywords = classificationKeyword.getChild(OAIElementName.KEYWORD, nodeNamespace);
        if (keywords != null) {
          List<Element> stringValus = keywords.getChildren(OAIElementName.STRING,
              nodeNamespace);

          for (Element element : stringValus) {
            elementValueList.add(element.getValue().trim());
          }
          mainJsonObject.put(OAIElementName.KEYWORDS, elementValueList);
        }
      }
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void learningResourceType(JSONObject mainJsonObject,
      Element nodeLomElement) {
    try {
      Element learningResourceType = nodeLomElement.getChild(OAIElementName.EDUCATIONAL,
          nodeNamespace);
      if (learningResourceType != null) {
        Element learningResourceTypeText = learningResourceType.getChild(
            OAIElementName.LEARNINGRESOURCETYPE, nodeNamespace);
        if (learningResourceTypeText != null) {
          Element value = learningResourceTypeText.getChild(
              OAIElementName.VALUE,
              nodeNamespace);
          if (value != null) {
            mainJsonObject.put("learningResourceType", value.getValue());
          }
        }
      }
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElementRight(JSONObject mainJsonObject,
      Element nodeLomElement) {
    try {
      Element rights = nodeLomElement.getChild(OAIElementName.RIGHTS, nodeNamespace).getChild(
          OAIElementName.DESCRIPTION, nodeNamespace);
      if (rights != null) {
        String rightText = rights.getChildText(OAIElementName.STRING, nodeNamespace).trim();

        mainJsonObject.put("license", rightText);
      }
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElementVersion(JSONObject mainJsonObject,
      Element nodeLomElement) {
    try {
      String version = nodeLomElement.getChild(OAIElementName.LIFECYCLE, nodeNamespace)
          .getChildText(
              OAIElementName.VERSION, nodeNamespace)
          .trim();
      mainJsonObject.put("version", version);
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  private void putElement(JSONObject mainJsonObject,
      Element nodeGeneralElements, String elementName, String map) {
    try {
      String elementValue = nodeGeneralElements.getChild(elementName, nodeNamespace)
          .getValue()
          .trim();
      mainJsonObject.put(map, elementValue);
    } catch (Exception e) {
      logger.debug(e.toString());
    }
  }

  public static Collection<File> listFileTree(File dir) {
    Set<File> fileTree = new HashSet<>();
    if (dir == null || dir.listFiles() == null) {
      return fileTree;
    }
    for (File entry : dir.listFiles()) {
      if (entry.isFile())
        fileTree.add(entry);
      else
        fileTree.addAll(listFileTree(entry));
    }
    return fileTree;
  }
}
