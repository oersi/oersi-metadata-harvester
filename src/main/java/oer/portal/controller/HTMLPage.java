package oer.portal.controller;

import oer.portal.config.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Controller
@Configuration
@EnableWebMvc
@ComponentScan
public class HTMLPage implements WebMvcConfigurer {

  @Autowired
  public ApplicationProperties application;

  /**
   * http://localhost:8080/templates/2019-Januar.html :-)
   * 
   * static String uploadDirectory = System.getProperty("user.home")
   **/
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    
    if (application != null) {
      registry.addResourceHandler("/templates/**")
          .addResourceLocations("file:" + application.getStatisticLocation().trim() + "//");
    }
  }

}
