package oer.portal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import oer.portal.converter.lom.rmif.ConvertLomToLRMIFormat;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@TestPropertySource(
    value = "file:${envConfigDir:envConf/unitTest/}configDirectoryLocation.properties")
@SpringBootTest
class OerImportConvertModulApplicationTest {

  @Autowired
  private ConvertLomToLRMIFormat convert;

  @Test
  void jsonElementMapper() {

    for (JSONObject jsonObject : convert.getListOfJSONObjects()) {

      getAssertEquals(jsonObject, 0, "license", "https://creativecommons.org/licenses/by/3.0");

      getAssertEquals(jsonObject, 1, "data", "LRMI");

      getArrayAssertEquals(jsonObject, 2, "keywords", "OER");

      getArrayAssertEquals(jsonObject, 3, "author", "André");

      getAssertEquals(jsonObject, 4, "inLanguage", "de_DE");

      getAssertEquals(jsonObject, 5, "abstract",
          "Das Tutorial vermittelt Studierenden Basis-Kompetenzen");

      getAssertEquals(jsonObject, 6, "learningResourceType", "image");

      getArrayAssertEquals(jsonObject, 7, "title", "OER Nutzungsmöglichkeiten");
      
      getAssertEquals(jsonObject, 8, "version", "1.0");

    }

  }

  public static void getAssertEquals(JSONObject jsonObject, int keyNumber, String key,
      String keyValue) {
    JSONArray keys = jsonObject.names();

    String keyNum = (String) keys.get(keyNumber);
    Object value = jsonObject.get(keyNum);

    assertEquals(keyNum, key);
    assertEquals(value, keyValue);
  }

  public static void getArrayAssertEquals(JSONObject jsonObject, int keyNumber, String key,
      String keyValue) {
    JSONArray keys = jsonObject.names();

    String keyNum = (String) keys.get(keyNumber);
    Object value = jsonObject.get(keyNum);

    assertEquals(keyNum, key);
    assertTrue(StringUtils.contains(value.toString(), keyValue));
  }

}
